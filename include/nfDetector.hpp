
#ifndef _NFDetector_HPP_
#define _NFDetector_HPP_

////////////////////////////////////////////////////////////////////
// Standard includes:
#include <vector>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
//#include "opencv2/nonfree/nonfree.hpp"
//#include <opencv2/xfeatures2d.hpp>
//#include <opencv2/xfeatures2d/nonfree.hpp>

#include "include/pose.hpp"

////////////////////////////////////////////////////////////////////
// File includes:
//#include "BGRAVideoFrame.h"
//#include "CameraCalibration.hpp"

////////////////////////////////////////////////////////////////////
using namespace cv;

class NFDetector
{
public:
	
  

  /**
   * Initialize a new instance of marker detector object
   * @calibration[in] - Camera calibration (intrinsic and distortion components) necessary for pose estimation.
   */  
   NFDetector(cv::Mat cameraMatrix, cv::Mat distCoeffs);
  
   void setTargetImage(Mat image, float targetWidth, float targetHeight);
  //! Searches for markes and fills the list of transformation for found markers
  void processFrame(const cv::Mat& frame);
  const Pose getPose() const;
  bool poseAvailable() { return _poseAvailable == true; }

  
protected:
	cv::Mat camMatrix;
	cv::Mat distCoeff;
	void detectKeypoints();
	void describeKeypoints();
	bool matchKeypoints();

  //! Calculates marker poses in 3D
  /// calculates the pose of the camera in the reference frame of the marker (markerCOS)  
	void estimatePosition();
	
	cv::Ptr<cv::Feature2D> _detectorDescriptor;
	
	cv::Ptr<DescriptorMatcher> _matcher;


	Mat _img_template;
	Mat _img_query;
	Pose _pose;
	bool _poseAvailable;

	float pxToObjectSpace; // conversion from pixels to metric units
	
	std::vector<KeyPoint> _keypoints_template;
	std::vector<KeyPoint> _keypoints_query;
	Mat _descriptors_template;
	Mat _descriptors_query;
	std::vector< DMatch > good_matches;

	std::vector<KeyPoint> good_keypoints_template, good_keypoints_query;

	std::vector<cv::Point3f> keypoint_locations_3d_template;
	std::vector<cv::Point2f> keypoint_locations_2d_query;

	float _targetWidth;
	float _targetHeight;

	float _targetWidthPx;
	float _targetHeightPx;

	//int minHessian;
};

#endif