#ifndef _POSE_HPP_
#define _POSE_HPP_ 1

#include <opencv2/core/core.hpp>



class PoseEstimator;
class PoseRefiner;
class Camera;
class Correspondences2D;


class Pose {
public:

	Pose();

	/**
	 * @brief projects 3D points into the camera image
	 * @param model points
	 * @param camera model
	 * @param resulting projected points
	 */
	//void project(const std::vector<cv::Point3f> &model, const Camera &camera, std::vector<cv::Point2f> &projected) const;

	/**
	 * @brief projects planar points into the camera image
	 * @param modelPlanar planar model points
	 * @param camera model
	 * @param resulting projected points
	 */
	//void project(const std::vector<cv::Point2f> &modelPlanar, const Camera &camera, std::vector<cv::Point2f> &projected) const;

	/**
	 * @brief retrieve 3D translation
	 * @return
	 */
	// 3 vector
	cv::Mat translation();
	void setTranslation(const cv::Mat &translation);
	//Eigen::Vector3d translation() const;
	//void setTranslation(const Eigen::Vector3d &translation); ///< set the translation component of the pose

	// rodrigues rotation vector:
	// http://docs.opencv.org/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#void Rodrigues(InputArray src, OutputArray dst, OutputArray jacobian)
	cv::Mat rotation() const;
	void setRotation(const cv::Mat &rotation); ///< set the rotation component of the pose
	
	/**
	 * @brief retrieve the rotation as a quaternion
	 * @return
	 */
	
	 //cv::Mat rotationAsQuat() const;
	 //void setRotationAsQuat(const cv::Mat &rotation); ///< set the rotation component of the pose
	//Eigen::Quaterniond rotation() const;
	//void setRotation(const Eigen::Quaterniond &rotation); ///< set the rotation component of the pose

	/**
	 * @brief converts the pose into an OpenGL style model view matrix
	 * @param mv
	 * @param datatype
	 */
	//void modelViewMatrix(cv::Mat& mv, int datatype = cv::DataType<double>::type) const;

	/**
	 * @brief returns an unit size homography
	 * @param camera
	 * @param targetSize
	 * @return 3x3 homography matrix
	 */
    //cv::Mat homography(const Camera &camera, const cv::Size2f &targetSize) const;

	/**
	 * @brief sets identity for pose
	 */
//	void setIdentity();

	/**
	 * @brief projects normalized planar points into the camera image
	 * @param modelPlanar planar points
	 * @param camera camera model
	 * @param targetSize physical size of target
	 * @param projected output (projected points)
	 */
	//void projectNormalized(const std::vector<cv::Point2f> &modelPlanar, const Camera &camera, const cv::Size2f &targetSize, std::vector<cv::Point2f> &projected) const;

    /*cv::Point2f projectPoint(const cv::Point3f &p,
                             const Camera &camera,
                             cv::OutputArray jacobian = cv::noArray()) const;

    void reprojectionErrors(const Correspondences2D &correspondences,
                            const Camera &camera,
                            std::vector<double> &sqError,
                            std::vector<cv::Point2f> &errorPoints) const;

*/
    std::string toString(unsigned int flag = 0) const;

    double score() const;
    void setScore(double score);


    /**
     * @brief calculate distance measures between this and another pose
     * @param other other pose to check
     * @param euclidian returns the euclidian distance
     * @param angular returns the angular distance
     */
  //  void distance(const Pose& other,double* euclidian,double* angular) const;

 //   void updatePose(const Pose& newPose,float interpolation);

    double lastTimeSeen() const;
    void setLastTimeSeen(double lastTimeSeen);



    /**
     * @brief get the projected area in the camera image
     * @param camera
     * @param targetSize
     * @return
     */
    /*cv::RotatedRect projectedArea(const Camera &camera,
                                  const cv::Vec3f &bounds,
                                  const cv::Size& imageSize);

*/
protected:
	cv::Mat _translation;
	cv::Mat _rotation;
  //  Eigen::Vector3d _translation;
    //Eigen::Quaterniond _rotation;

    double              _score;
    double              _lastTimeSeen;

	//friend class PoseEstimator;
	//friend class PoseRefiner;
};



#endif
