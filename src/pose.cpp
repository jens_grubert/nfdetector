
#include "include/pose.hpp"
//#include "sstt/correspondences.hpp"
//#include "sstt/camera.hpp"




//#include <opencv2/core/eigen.hpp>

#include <sstream>


cv::Mat Pose::translation()
{
	return _translation;
}

void Pose::setTranslation(const cv::Mat &translation)
{
	_translation = translation;
}

cv::Mat Pose::rotation() const
{
	return _rotation;
}

void Pose::setRotation(const cv::Mat &rotation)
{
	_rotation = rotation;
}
/*
void Pose::modelViewMatrix(cv::Mat &mv,int datatype) const
{
	// Rt part
	cv::Mat Rt = cv::Mat::eye(4,4,cv::DataType<double>::type);

	cv::Mat Rm,T;

	cv::eigen2cv(_rotation.toRotationMatrix(),Rm);
	cv::eigen2cv(_translation,T);

	// copy the pose
	for (int r = Rm.rows; r-->0;) {
		for (int c = Rm.cols; c-->0;)
			Rt.at<double>(r,c) = Rm.at<double>(r,c);
		Rt.at<double>(r,3) = T.at<double>(r,0);
	}

	//! @todo combine rotation and flip
	// OpenGL has reversed Y & Z coords
	cv::Mat reverseYZ = cv::Mat::eye(4, 4, cv::DataType<double>::type);
	reverseYZ.at<double>(1, 1) = reverseYZ.at<double>(2,2) = -1;

	// rotate around X to make Y up on target
	cv::Mat Yup = cv::Mat::zeros(4, 4, cv::DataType<double>::type);
	Yup.at<double>(0,0) = Yup.at<double>(3,3) = 1;
	Yup.at<double>(2,1) = -1;
	Yup.at<double>(1,2) =  1;

	// chain them up
	mv = reverseYZ * Rt * Yup;

	// transpose
	mv = mv.t();


//    std::cout << mv << std::endl;


////    Eigen::Matrix3d rot = _rotation.toRotationMatrix();

//    Eigen::Matrix3d revYZ; revYZ.setIdentity();
//    Eigen::Matrix3d yUp; yUp.setZero();
//    yUp(0,0) =  1;
//    yUp(1,2) = -1;
//    yUp(2,1) =  1;


//    Eigen::Matrix3d rotRot = revYZ * _rotation.toRotationMatrix() * yUp;
//    rotRot.transposeInPlace();

//    std::cout << rotRot << std::endl;




}

*/
Pose::Pose()
{
	//setIdentity();
}
/*
void
Pose::reprojectionErrors(const Correspondences2D& correspondences,
                         const Camera& camera,
                         std::vector<double>& sqError,
                         std::vector<cv::Point2f>& errorPoints) const
{
    std::vector<cv::Point2f> projectedPoints; projectedPoints.reserve(correspondences.size());


    cv::Size2f tSizeHack(10.24f,7.68f);

    projectNormalized(correspondences.from(),camera,tSizeHack,projectedPoints);

    sqError.clear();
    sqError.resize(correspondences.size(),std::numeric_limits<double>::max());

    errorPoints.clear();
    errorPoints.resize(correspondences.size(),
                       cv::Point2f(std::numeric_limits<float>::max(),
                                   std::numeric_limits<float>::max()));

    for (register size_t i = projectedPoints.size();i-->0;) {

        errorPoints[i] = correspondences.to()[i] - projectedPoints[i];
        sqError[i] = correspondences.to()[i].ddot(projectedPoints[i]);
    }

}

*/
/*
std::string Pose::toString(unsigned int flag) const
{
    std::stringstream ss;
    Eigen::AngleAxis<double> aa(_rotation);
    cv::Mat t,r;

    if (flag & 1) {
        cv::eigen2cv(_translation,t);
        ss << t << " ";
    }

    if (flag & 2) {

        cv::eigen2cv(aa.axis(),r);
        ss << aa.angle() << " " << r;
    }

    if (flag & 4) {

        Eigen::Vector3d ea = _rotation.matrix().eulerAngles(2,0,2);
        ea(0) *= 180/CV_PI;
        ea(1) *= 180/CV_PI;
        ea(2) *= 180/CV_PI;

        cv::eigen2cv(ea,r);


        ss << r;
    }

    return ss.str();
}
*/
double Pose::score() const
{
    return _score;
}

void Pose::setScore(double score)
{
    _score = score;
}
/*
void Pose::distance(const Pose &other, double *euclidian, double *angular) const
{
    if (euclidian) {
        *euclidian = (this->translation()-other.translation()).norm();
    }

    if (angular) {
        *angular = other.rotation().angularDistance(this->rotation());
    }
}
*/
/*
void Pose::updatePose(const Pose &newPose, float interpolation)
{
    std::cout << toString(1) << std::endl;

    Eigen::Vector3d deltaT = newPose.translation()-translation();

    deltaT *= interpolation;

    _translation = _translation + deltaT;

    std::cout << toString(1) << std::endl;
}
*/
double Pose::lastTimeSeen() const
{
    return _lastTimeSeen;
}

void Pose::setLastTimeSeen(double lastTimeSeen)
{
    _lastTimeSeen = lastTimeSeen;
}



/*
cv::Point2f
Pose::projectPoint(const cv::Point3f& p,
                   const Camera& camera,
                   cv::OutputArray jacobian) const
{
    cv::Mat rotMat,transMat;

    cv::eigen2cv(_rotation.toRotationMatrix(),rotMat);
    cv::eigen2cv(_translation,transMat);

    std::vector<cv::Point3f> model(1);
    std::vector<cv::Point2f> projected(1);
    model[0] = p;

    cv::projectPoints(model,
                      rotMat,
                      transMat,
                      camera.intrinsics(),
                      camera.getDistortion(),
                      projected,
                      jacobian);

    return projected[0];
}


void
Pose::project(const std::vector<cv::Point3f>& model,
			  const Camera& camera,
			  std::vector<cv::Point2f>& projected) const
{
    projected.clear(); projected.reserve(model.size());

	cv::Mat rotMat,transMat;

	cv::eigen2cv(_rotation.toRotationMatrix(),rotMat);
    cv::eigen2cv(_translation,transMat);

	cv::projectPoints(model,
					  rotMat,
					  transMat,
					  camera.intrinsics(),
					  camera.getDistortion(),
					  projected);
}

void
Pose::project(const std::vector<cv::Point2f>& modelPlanar,
			  const Camera& camera,
			  std::vector<cv::Point2f>& projected) const
{
	std::vector<cv::Point3f> model3D;
	PointTools::convert(modelPlanar,model3D,0);
	this->project(model3D,camera,projected);
}

cv::RotatedRect
Pose::projectedArea(const Camera& camera,
                    const cv::Vec3f& bounds,
                    const cv::Size& imageSize)

{
    std::vector<cv::Point3f> objectPoints(4);

    cv::Vec3f nS = bounds * .5f;

    objectPoints[0] = cv::Point3f( -nS(0), -nS(1), 0.f);
    objectPoints[1] = cv::Point3f(  nS(0), -nS(1), 0.f);
    objectPoints[2] = cv::Point3f(  nS(0),  nS(1), 0.f);
    objectPoints[3] = cv::Point3f( -nS(0),  nS(1), 0.f);

    std::vector<cv::Point2f> projected;

    project(objectPoints,camera,projected);

////    cv::Vec2f deltaS( (float) camera.getSize().width  / imageSize.width,
////                      (float) camera.getSize().height / imageSize.height);

//    cv::Vec2f deltaS( (float) imageSize.width  / camera.getSize().width,
//                      (float) imageSize.height / camera.getSize().height );

//    for (int i = projected.size();i-->0;) {
//        projected[i].x *= deltaS(0);
//        projected[i].y *= deltaS(1);
//    }

//    std::cout << "Delta S" << deltaS << " " << imageSize << std::endl;


    return cv::minAreaRect(projected);

}

void
Pose::projectNormalized(const std::vector<cv::Point2f>& modelPlanar,
						const Camera& camera,
						const cv::Size2f& targetSize,
						std::vector<cv::Point2f>& projected) const
{
	std::vector<cv::Point3f> objectPoints(modelPlanar.size());
    for (register size_t i = modelPlanar.size();i-->0;) {
		objectPoints[i] = cv::Point3f(
					(modelPlanar[i].x-.5f) * targetSize.width,
					(modelPlanar[i].y-.5f) * targetSize.height,
					0.f
					);
	}
	this->project(objectPoints,camera,projected);

    PointTools::unitScale<true>(projected,camera.getSize());
}

cv::Mat
Pose::homography(const Camera& camera,const cv::Size2f& targetSize) const
{

	std::vector<cv::Point3f> objectPoints(4);
	std::vector<cv::Point2f> imagePoints(4),objectPointsPlanar(4);

    cv::Size2f targetHalfSize(targetSize);
    targetHalfSize.width *= .5f; targetHalfSize.height *= .5f;

    objectPoints[0] = cv::Point3f(-targetHalfSize.width,-targetHalfSize.height,0);
    objectPoints[1] = cv::Point3f( targetHalfSize.width,-targetHalfSize.height,0);
    objectPoints[2] = cv::Point3f( targetHalfSize.width, targetHalfSize.height,0);
    objectPoints[3] = cv::Point3f(-targetHalfSize.width, targetHalfSize.height,0);

	this->project(objectPoints,camera,imagePoints);

	PointTools::unitScale<true>(imagePoints,camera.getSize());

    cv::Size2f size = PointTools::unitScaleSize(targetSize);

	objectPointsPlanar[0] = cv::Point2f(0,0);
    objectPointsPlanar[1] = cv::Point2f(size.width,0);
    objectPointsPlanar[2] = cv::Point2f(size.width,size.height);
    objectPointsPlanar[3] = cv::Point2f(0,size.height);

	return cv::findHomography(objectPointsPlanar,imagePoints,0);

}
*/

//void Pose::setIdentity()
//{
//	_translation << 0,0,0;
//	_rotation.Identity();
//}


