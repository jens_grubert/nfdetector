
////////////////////////////////////////////////////////////////////
// Standard includes:
#include <iostream>
#include <sstream>

////////////////////////////////////////////////////////////////////
#include "opencv2/imgproc.hpp"

// File includes:
#include "include/nfDetector.hpp"

#include <ctime>

using namespace std;
using namespace cv;
//using namespace cv::xfeatures2d;


#define SHOW_DEBUG_IMAGES

NFDetector::NFDetector(cv::Mat cameraMatrix, cv::Mat distCoeffs)
    
{
	camMatrix = cameraMatrix;
	distCoeff = distCoeffs;
	
	// create detector + descriptor and an appropriate Matcher (nearest neigbhor or brute force)

	_detectorDescriptor = ORB::create();
	//_detectorDescriptor = AKAZE::create();
	_matcher = DescriptorMatcher::create("BruteForce-Hamming");

	//_detectorDescriptor = KAZE::create();
	//_matcher = new cv::FlannBasedMatcher();
	
	// for alternative detectors and descriptors see:
	//http://docs.opencv.org/trunk/d0/d13/classcv_1_1Feature2D.html
	
	// You can also combine individual dectors and descriptors
	// see freak example here: https://github.com/kikohs/freak/blob/master/demo/freak_demo.cpp

	_poseAvailable = false;
#ifdef SHOW_DEBUG_IMAGES
	cv::namedWindow("Matches", CV_WINDOW_NORMAL);
#endif
}

void NFDetector::processFrame(const cv::Mat& frame)
{
	clock_t begin = clock();

	_img_query = frame.clone();
	detectKeypoints();
	describeKeypoints();
	if (matchKeypoints()) {
		estimatePosition();		
	}
	else {
		_poseAvailable = false;
	}

	clock_t end = clock();
	double elapsed_msecs = double(end - begin) / CLOCKS_PER_SEC * 1000;
	cout << "elapsed msecs: " << elapsed_msecs << endl;

}

const Pose NFDetector::getPose() const
{
    return _pose;
}


void NFDetector::setTargetImage(Mat image,  float targetWidth, float targetHeight) {
	_targetWidth = targetWidth;
	_targetHeight = targetHeight;
	_img_template = image;

	_targetWidthPx = image.cols;
	_targetHeightPx = image.rows;
	
	pxToObjectSpace = _targetWidth / _targetWidthPx;
		
	_detectorDescriptor->detect(_img_template, _keypoints_template);
	_detectorDescriptor->compute(_img_template, _keypoints_template, _descriptors_template);
	//detector->detect(_img_template, _keypoints_template);	
	//extractor.compute(_img_template, _keypoints_template, _descriptors_template);
}

void NFDetector::detectKeypoints()
{
	_detectorDescriptor->detect(_img_query, _keypoints_query);
}
void NFDetector::describeKeypoints()
{
	_detectorDescriptor->compute(_img_query, _keypoints_query, _descriptors_query);
}
bool NFDetector::matchKeypoints() 
{
	std::vector< DMatch > matches;
	_matcher->match(_descriptors_query, _descriptors_template, matches);
	double max_dist = 0; double min_dist = 100;

	//-- Quick calculation of max and min distances between keypoints
	//for( int i = 0; i < _descriptors_template.rows; i++ )
	for (int i = 0; i < _descriptors_query.rows; i++)
	{
		double dist = matches[i].distance;
		if (dist < min_dist) min_dist = dist;
		if (dist > max_dist) max_dist = dist;
	}

	//-- keep only "good" matches (i.e. whose distance is less than n*min_dist )
	// also extract the keypoint locations in the query image and in the template

	good_matches.clear();
	good_keypoints_template.clear();
	good_keypoints_query.clear();
	keypoint_locations_3d_template.clear();
	keypoint_locations_2d_query.clear();
	//for( int i = 0; i < _descriptors_template.rows; i++ )
	for (int i = 0; i < _descriptors_query.rows; i++)
	{
		if (matches[i].distance < 2.5 * min_dist)
		{
			good_matches.push_back(matches[i]);
		}
	}

	if (good_matches.size() < 4) {
		return false;
	}

	for (int i = 0; i < good_matches.size(); ++i) {
		// get keypoint location for template image
		good_keypoints_template.push_back(_keypoints_template[good_matches[i].trainIdx]);
		// and for the query image
		good_keypoints_query.push_back(_keypoints_query[good_matches[i].queryIdx]);

		cv::Point3f p3dTemplate = cv::Point3f((_keypoints_template[good_matches[i].trainIdx].pt.x - 0.5*_targetWidthPx)*pxToObjectSpace, (_keypoints_template[good_matches[i].trainIdx].pt.y - 0.5*_targetHeightPx)*pxToObjectSpace, 0.0f);
		cv::Point2f p2dQuery = cv::Point2f(_keypoints_query[good_matches[i].queryIdx].pt.x, _keypoints_query[good_matches[i].queryIdx].pt.y);

		keypoint_locations_3d_template.push_back(p3dTemplate);
		keypoint_locations_2d_query.push_back(p2dQuery);

	}

#ifdef SHOW_DEBUG_IMAGES
	Mat img_matches;
	/*drawMatches( img_template, _keypoints_template, img_query, _keypoints_query,
	good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
	vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );*/

	cv::drawMatches(_img_query, _keypoints_query, _img_template, _keypoints_template,
		good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
		vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

	//-- Localize the object
	std::vector<Point2f> obj;
	std::vector<Point2f> scene;

	for (int i = 0; i < good_matches.size(); i++)
	{
		//-- Get the keypoints from the good matches
		obj.push_back(_keypoints_template[good_matches[i].trainIdx].pt);
		scene.push_back(_keypoints_query[good_matches[i].queryIdx].pt);
	}

	Mat H = findHomography(obj, scene, CV_RANSAC);

	//-- Get the corners from the image_1 ( the object to be "detected" )
	std::vector<Point2f> obj_corners(4);
	obj_corners[0] = cvPoint(0, 0); obj_corners[1] = cvPoint(_img_template.cols, 0);
	obj_corners[2] = cvPoint(_img_template.cols, _img_template.rows); obj_corners[3] = cvPoint(0, _img_template.rows);
	std::vector<Point2f> scene_corners(4);

	cv::perspectiveTransform(obj_corners, scene_corners, H);

	//-- Draw lines between the corners (the mapped object in the scene - image_2 )
	
	cv::line(img_matches, scene_corners[0], scene_corners[1], Scalar(0, 255, 0), 4);
	cv::line(img_matches, scene_corners[1], scene_corners[2], Scalar(0, 255, 0), 4);
	cv::line(img_matches, scene_corners[2], scene_corners[3], Scalar(0, 255, 0), 4);
	cv::line(img_matches, scene_corners[3], scene_corners[0], Scalar(0, 255, 0), 4);

	//-- Show detected matches
	cv::imshow("Matches", img_matches);
#endif

	return true;

}

void NFDetector::estimatePosition()
{	
    cv::Mat Rvec;
    cv::Mat_<float> Tvec;
    cv::Mat raux,taux;
	cv::solvePnP(keypoint_locations_3d_template, keypoint_locations_2d_query, camMatrix, distCoeff, raux, taux);
    raux.convertTo(Rvec,CV_32F);
    taux.convertTo(Tvec ,CV_32F);

    cv::Mat_<float> rotMat(3,3); 
    cv::Rodrigues(Rvec, rotMat);

    // Copy to transformation matrix
    for (int col=0; col<3; col++)
    {
        for (int row=0; row<3; row++)
        { 
			_pose.setRotation(Rvec);				
        }
		_pose.setTranslation(Tvec);
    }
	_poseAvailable = true;
	//	std::cout << "cur trans: " << Tvec << endl << " cur rot: " << Rvec << endl;
 
}
